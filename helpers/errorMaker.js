module.exports = (status, message) => {
  return new Promise((resolve, reject) => {
    const error = new Error(message || 'internal server error')
    error.status = status || 500
    reject(error)
  })
}
