module.exports = (req, res) => {
  res.json({
    hello: 'world',
    body: req.body,
    query: req.query,
    cookies: req.cookies,
    method: req.method
  })
}
