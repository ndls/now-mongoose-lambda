const db = require('db')
module.exports = async (req, res) => {
  try {
    res.json(await db.example.create(require('../../helpers/seedName')()))
    // await require('./helpers/errorMaker')(666, 'hail satain')
  } catch (error) {
    require('../../handlers/errorHandler')(error, req, res)
  }
}
