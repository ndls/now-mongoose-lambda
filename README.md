## Mongoose lambda for Now 2.0

A working solution for using [Mongoose](https://mongoosejs.com/) with lambdas on [Zeit's Now 2.0 platform](https://zeit.co/now).
