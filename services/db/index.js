const mongoose = require('mongoose')
if (!process.env.MONGO_URL) {
  throw new Error('missing env MONGO_URL')
}
let client = null

async function connector () {
  if (client === null) {
    client = await mongoose.createConnection(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      bufferCommands: false,
      bufferMaxEntries: 0,
      dbName: 'example'
    }).catch(error => {
      client = null
      return Promise.reject(error)
    })
  }
}
module.exports = {
  example: {
    create: async (obj) => {
      await connector()
      if (!client.models.Example) client.model(require('./Example').name, new mongoose.Schema(require('./Example').schema))
      const Example = client.model(require('./Example').name)
      const example = new Example(obj)
      return example.save()
    },
    get: async (obj) => {
      await connector()
      if (!client.models.Example) client.model(require('./Example').name, new mongoose.Schema(require('./Example').schema))
      return client.model(require('./Example').name).find(obj)
    }
  }
}
